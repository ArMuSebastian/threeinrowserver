// swift-tools-version:5.5.0
import PackageDescription

let package = Package(
    name: "ThreeInRowServer",
    platforms: [
       .macOS(.v10_15)
    ],
    dependencies: [
        .package(url: "https://gitlab.com/ArMuSebastian/ThreeInRowCore", branch: "master"),
        .package(url: "https://github.com/vapor/vapor.git", from: "4.0.0"),
        
        .package(url: "https://github.com/vapor/websocket-kit.git", from: "2.0.0"),
        
        .package(url: "https://github.com/vapor/fluent.git", from: "4.0.0"),
        .package(url: "https://github.com/vapor/fluent-postgres-driver.git", from: "2.0.0"),
        //        .package(url: "https://github.com/vapor/fluent-mongo-driver.git", from: "1.0.0"),
        //        .package(url: "https://github.com/vapor/fluent-mysql-driver.git", from: "4.0.0"),
        //        .package(url: "https://github.com/vapor/fluent-sqlite-driver.git", from: "4.0.0")
        
    ],
    targets: [
        .target(
            name: "UserKit",
            dependencies: [
                .product(name: "Vapor", package: "vapor"),
                .product(name: "Fluent", package: "fluent")
            ]
        ),

        .target(
            name: "App",
            dependencies: [
                .product(name: "Vapor", package: "vapor"),
                
                .product(name: "WebSocketKit", package: "websocket-kit"),

                .product(name: "Fluent", package: "fluent"),
                .product(name: "FluentPostgresDriver", package: "fluent-postgres-driver"),
//                .product(name: "FluentMongoDriver", package: "fluent-mongo-driver"),
//                .product(name: "FluentMySQLDriver", package: "fluent-mysql-driver"),
//                .product(name: "FluentSQLiteDriver", package: "fluent-sqlite-driver"),
                    .target(name: "UserKit")
            ],
            swiftSettings: [
                .unsafeFlags(["-cross-module-optimization"], .when(configuration: .release))
            ]
        ),
        .executableTarget(
            name: "Run",
            dependencies: [
                .target(name: "App")
            ]
        ),
        .testTarget(name: "AppTests", dependencies: [
            .target(name: "App"),
            .product(name: "XCTVapor", package: "vapor"),
        ])
    ]
)
