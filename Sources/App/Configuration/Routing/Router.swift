import Vapor

import struct UserKit.UserController

enum Router: CaseIterable {

    case user

    var raw: RouteCollection {
        switch self {
        case .user:
            return UserKit.UserController()
        }
    }

}

extension Router {

    static func route(for app: Application) throws {

        try Router.allCases
            .map(\.raw)
            .forEach(app.register(collection:))

    }

}
