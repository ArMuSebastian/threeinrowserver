import Vapor

public func configure(_ app: Application) throws {

    app.logger.logLevel = .debug

    try DataBase.register(for: app)
    try DataBaseMigration.migrate(for: app)

    try app.autoMigrate().wait()

    try Router.route(for: app)
}
