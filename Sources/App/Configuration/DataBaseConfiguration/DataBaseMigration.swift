import Vapor
import FluentKit

import struct UserKit.UserMigration

enum DataBaseMigration: CaseIterable {

    case user

    var raw: Migration {
        switch self {
        case .user:
            return UserKit.UserMigration()
        }
    }

    var id: DatabaseID? {
        return nil
    }

}

extension DataBaseMigration {

    static func migrate(for app: Application) throws {

        DataBaseMigration.allCases.forEach { migration in
            app.migrations.add(
                migration.raw,
                to: migration.id
            )
        }

    }

}
