import Fluent
import FluentPostgresDriver
import Vapor

enum DataBase: CaseIterable {

    case postgres

    var configuration: DatabaseConfigurationFactory {
        switch self {
        case .postgres:
            return .postgres(
                hostname: Environment.get("DATABASE_HOST") ?? "localhost",
                username: Environment.get("DATABASE_USERNAME") ?? "vapor_username",
                password: Environment.get("DATABASE_PASSWORD") ?? "vapor_password",
                database: Environment.get("DATABASE_NAME") ?? "vapor_database"
            )
        }
    }

    var id: DatabaseID {
        switch self {
        case .postgres:
            return .psql
        }
    }

    var isDefault: Bool? {
        return nil
    }

}

extension DataBase {

    static func register(for app: Application) throws {

        DataBase.allCases.forEach { dataBase in
            app.databases.use(
                dataBase.configuration,
                as: dataBase.id,
                isDefault: dataBase.isDefault
            )
        }

    }

}
