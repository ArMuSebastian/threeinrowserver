import Foundation
import Vapor

public struct UserController {

    var assosiatedEndpoint: String = "user"

    public init() {}

}

extension UserController: RouteCollection {

    public func boot(routes: RoutesBuilder) throws {
        let userRoutes = routes.grouped("v1", .constant(assosiatedEndpoint))

        // GET
//        acronymsRoutes.get(use: getAllHandler)
//        acronymsRoutes.get(":acronymID", use: getHandler)
//        acronymsRoutes.get("search", use: searchHandler)
//        acronymsRoutes.get("first", use: getFirstHandler)
//        acronymsRoutes.get("sorted", use: sortedHandler)
        // POST
//        userRoutes.post(use: create)
        userRoutes.get(use: get)
        // PUT
//        acronymsRoutes.put(":acronymID", use: updateHandler)
//        // DELETE
//        acronymsRoutes.delete(":acronymID", use: deleteHandler)

    }

}

extension UserController {

    func get(_ req: Request) throws -> EventLoopFuture<User.PublicUser> {
        let user = User.PrivateUser(name: "kek", nickname: "kek", password: "kek")

        return user.save(on: req.db)
            .map { User.PublicUser(from: user) }
    }

}

extension UserController {

//    func create(_ req: Request) throws -> EventLoopFuture<User> {
//        let user = try req.content.decode(User.PublicUser.self)
//        user.password = try Bcrypt.hash(user.password)
//
//        return user.save(on: req.db)
//            .map { user }
//    }

}
//
//extension AcronymsController {
//
//    func updateHandler(_ req: Request) throws -> EventLoopFuture<Acronym> {
//        let newAcronym = try req.content.decode(Acronym.self)
//
//        return Acronym.find(req.parameters.get("acronymID"), on: req.db)
//            .unwrap(or: Abort(.badRequest))
//            .map { oldAcronym -> Acronym in
//                oldAcronym.short = newAcronym.short
//                oldAcronym.long = newAcronym.long
//                return oldAcronym
//            }.flatMap { acronym -> EventLoopFuture<Acronym> in
//                acronym.save(on: req.db)
//                    .map { _ in
//                        return acronym
//                    }
//            }
//    }
//
//}
//
//extension AcronymsController {
//
//    func deleteHandler(_ req: Request) throws -> EventLoopFuture<Acronym> {
//        
//        return Acronym.find(req.parameters.get("acronymID"), on: req.db)
//            .unwrap(or: Abort(.badRequest))
//            .flatMap { acronym in
//                return acronym.delete(on: req.db)
//                    .map { _ in
//                        return acronym
//                    }
//            }
//    }
//
//}
