import Fluent
import Vapor

extension Token {

    final
    internal class PrivateToken: Model, Content, Authenticatable {

        static
        internal var schema: String = DatabaseConfiguration.schema

        @ID
        internal var id: UUID?

        @Field(key: .string(Token.DatabaseConfiguration.valueKey))
        internal var value: String

        @Parent(key: .string(Token.DatabaseConfiguration.holderKey))
        internal var holder: Token.DatabaseConfiguration.Parent.PrivateUser

        internal init() {}

        internal init(
            id: UUID? = nil,
            value: String,
            holder: Token.DatabaseConfiguration.Parent.PrivateUser.IDValue
        ) {
            self.value = value
            self.$holder.id = holder
        }

    }

}

extension Token.PrivateToken: ModelAuthenticatable {

    static var usernameKey: KeyPath<Token.PrivateToken, Field<String>> {
        return \Self.$value
    }

    static var passwordHashKey: KeyPath<Token.PrivateToken, Field<String>> {
        let w = \Token.PrivateToken.$holder.password
        fatalError()
//        return \Self.holder.password
    }

    func verify(password: String) throws -> Bool {
        return true
    }

}
