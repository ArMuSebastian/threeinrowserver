public enum Token {

}

extension Token {

    public enum DatabaseConfiguration {

        internal typealias Parent = User

        static
        public let schema: String = "users"

        static
        public let valueKey: String = "value"

        static
        public let holderKey: String = "holder"

    }

}
