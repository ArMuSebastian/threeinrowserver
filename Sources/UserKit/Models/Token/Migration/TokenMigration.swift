import Foundation
import Fluent

public struct TokenMigration {

    private typealias MigrationEntity = Token.DatabaseConfiguration

    public init() {}

}

extension TokenMigration: Migration {

    public func prepare(on database: Database) -> EventLoopFuture<Void> {
        return database.schema(MigrationEntity.schema)
            .id()
            .field(
                .definition(
                    name: .custom(MigrationEntity.valueKey),
                    dataType: .string,
                    constraints: []
                )
            )
            .field(
                .definition(
                    name: .custom(MigrationEntity.holderKey),
                    dataType: .uuid,
                    constraints:
                        [
                            .required,
                            .references(
                                MigrationEntity.Parent.DatabaseConfiguration.schema,
                                .id,
                                onDelete: .cascade
                            )
                        ]
                )
            )
            .create()
    }

    public func revert(on database: Database) -> EventLoopFuture<Void> {
        return database.schema(MigrationEntity.schema)
            .delete()
    }

}
