import Fluent
import Vapor

extension User {

    public struct PublicUser: Content {

        public let name: String
        public let nickname: String

    }

}

extension User.PublicUser {

    internal init(from user: User.PrivateUser) {
        self.init(
            name: user.name,
            nickname: user.nickname
        )
    }

}
