import Foundation
import Fluent

public struct UserMigration {

    private typealias MigrationEntity = User.DatabaseConfiguration

    public init() {}

}

extension UserMigration: Migration {

    public func prepare(on database: Database) -> EventLoopFuture<Void> {
        return database.schema(MigrationEntity.schema)
            .id()
            .field(.definition(name: .custom(MigrationEntity.nameKey), dataType: .string, constraints: []))
            .field(.definition(name: .custom(MigrationEntity.nicknameKey), dataType: .string, constraints: []))
            .unique(on: .string(MigrationEntity.nicknameKey))
            .field(.definition(name: .custom(MigrationEntity.passwordKey), dataType: .string, constraints: [.required]))
            .create()
    }

    public func revert(on database: Database) -> EventLoopFuture<Void> {
        return database.schema(MigrationEntity.schema)
            .delete()
    }

}
