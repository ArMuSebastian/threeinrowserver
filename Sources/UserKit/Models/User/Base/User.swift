public enum User {

}

extension User {

    public enum DatabaseConfiguration {

        static
        public let schema: String = "users"

        static
        public let nameKey: String = "name"
        static
        public let nicknameKey: String = "nickname"

        static
        public let passwordKey: String = "password"

    }

}
