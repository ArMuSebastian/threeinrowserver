import Fluent
import Vapor

extension User {

    final
    internal class PrivateUser: Model, Content, Authenticatable {

        static
        internal var schema: String = DatabaseConfiguration.schema

        @ID
        internal var id: UUID?

        @Field(key: .string(User.DatabaseConfiguration.nameKey))
        internal var name: String

        @Field(key: .string(User.DatabaseConfiguration.nicknameKey))
        internal var nickname: String

        @Field(key: .string(User.DatabaseConfiguration.passwordKey))
        internal var password: String

        internal init() {}

        internal init(
            id: UUID? = nil,
            name: String,
            nickname: String,
            password: String
        ) {
            self.name = name
            self.nickname = nickname
            self.password = password
        }

    }

}
